execute pathogen#infect()
syntax enable

filetype on
filetype plugin on
filetype indent off

let mapleader=","

set noerrorbells
set visualbell

set title
set showmode

set clipboard=unnamed
set mouse=a
set nobackup
set nowritebackup
set noswapfile
set backupcopy=yes
set autoread

set hidden
set lazyredraw
set ttyfast

"set number
set incsearch
set ignorecase

set encoding=utf-8
set fileencoding=utf-8
set fileformat=unix
set fileformats=unix,dos

set nowrap
set linebreak

set backspace=indent,eol,start  " allow backspacing over everything.
set esckeys                     " Allow cursor keys in insert mode.
set nostartofline               " Make j/k respect the columns
set timeoutlen=500              " how long it wait for mapped commands
set ttimeoutlen=100             " faster timeout for escape key and others

set shiftwidth=4
set tabstop=4
set expandtab
set autoindent
"set smartindent

"let loaded_matchparen=1 " Don't load matchit.vim (paren/bracket matching)
set noshowmatch         " Don't match parentheses/brackets
set nocursorline        " Don't paint cursor line
set nocursorcolumn      " Don't paint cursor column
set lazyredraw          " Wait to redraw
set scrolljump=8        " Scroll 8 lines at a time at bottom/top
let html_no_rendering=1 " Don't render italic, bold, links in HTML
:set ttyfast

set splitbelow
set splitright

autocmd QuickFixCmdPost * nested cwindow

set background=light

if has("gui_running")
    set guioptions-=m  " remove menu bar
    set guioptions-=T  " remove toolbar
    set guioptions-=L  " remove left-hand scroll bar
    set lines=40 columns=120
    set guifont=Monaco:h13
    set guicursor+=n-v-c-i:blinkon0
end


"Plugins"
let g:ctrlp_extensions = ['line']
let g:ctrlp_custom_ignore = '\v[\/](node_modules|target|dist)|(\.(swp|ico|git|svn|svg|jpg|png|gif|ogg|mp3|wav))$'
let g:ctrlp_working_path_mode = 0
let g:ctrlp_match_window = 'results:7'

let g:jsx_ext_required = 0

let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0
let g:syntastic_javascript_checkers = ['eslint']
let g:syntastic_javascript_eslint_exec = 'node_modules/.bin/eslint'
let g:syntastic_javascript_eslint_args = '--quiet'
let g:syntastic_mode_map = { 'mode': 'passive', 'active_filetypes': [],'passive_filetypes': [] }

let g:AutoPairsMapCR = 0

let g:NERDTreeQuitOnOpen = 1
let NERDTreeIgnore=['.DS_Store']
let NERDTreeShowBookmarks=0         "show bookmarks on startup
let NERDTreeHighlightCursorline=1   "Highlight the selected entry in the tree
let NERDTreeShowLineNumbers=0
let NERDTreeMinimalUI=1

let g:acp_behaviorKeywordLength = 2
let g:acp_behaviorFileLength = 2

let g:acp_completeOption='.,w,b,u,i,t'
set complete=.,w,b,u,i,t
set completeopt=longest,menuone
set pumheight=5

"end plugins"

"Functions"
function! CrHandler()
  let pr = strpart(getline('.'), col('.')-2, 1)
  let nx = strpart(getline('.'), col('.')-1, 1)
  if ( (pr == "{" && nx == "}") || (pr == "[" && nx == "]") || (pr =="(" && nx == ")") )
    return "\<CR>\<ESC>\<UP>o\<TAB>"
  else
    return "\<CR>"
  endif
endfunction

function! TabOrComplete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
"end Functions"


"Keymap"
inoremap jj <ESC>
inoremap jk <ESC>
nnoremap <C-e> 3<C-e>
nnoremap <C-y> 3<C-y>
inoremap <CR> <C-r>=CrHandler()<CR>
inoremap <Tab> <C-r>=TabOrComplete()<CR>
nnoremap <leader>n :NERDTreeFind <CR>
map <c-t> :CtrlPLine <CR>
map <leader>t :CtrlPLine <CR>
nnoremap <c-b> :CtrlPBuffer <CR>
nnoremap <leader>b :CtrlPBuffer <CR>
nnoremap <leader>f :CtrlP <CR>

"end Keymap"

"Commands"
command Wd write|bdelete
"end Commands"

"File handlers"
autocmd BufNewFile,BufReadPost *.coffee setl shiftwidth=2 tabstop=2
"end File handlers"
